# Noita Emotes

![](https://i.imgur.com/OE7t3EV.mp4)
  
An emote system for Noita. :)  
  
* Noita beta branch (With new input mod API functions): Invoke with custom input, set in options menu (Q key by default).  
* Noita main branch (Prior to input mod API functions): Invoke with A+S+D keys.  
* [M-Néé](https://modworkshop.net/mod/37673) integration: Invoke with custom key set in M-Néé menu (Q key by default).  
* Compatible with Noita Together! Just make sure "Noita Emotes" is listed above "Noita Together" in the Mods menu.  
* Compatible with Noita Arena! Just make sure "Noite Emotes" is listed above "Noita Arena" in the Mods menu.  
* Ability to support all of the in-game cosmetics during emotes (crown, amulet, gem), and even spawn arbitrary entities (Like particle effects).  
* Emotes are stored in a manner such that modders and artists can easily contribute (Expand the emotes_list table in files/scripts/emotes_list.lua).  
  
Pull requests for emote additions are welcome.

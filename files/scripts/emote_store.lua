if EMOTE_STORE then
    return
end
dofile("mods/noita-emotes/files/scripts/stringstore/stringstore.lua")
dofile("mods/noita-emotes/files/scripts/stringstore/noitaglobalstore.lua")
EMOTE_STORE = stringstore.open_store(stringstore.noita.global("EMOTE_STORE"))

if (EMOTE_STORE.initialized ~= true) then
    EMOTE_STORE.initialized = true
    EMOTE_STORE.emote_state = "none"
    EMOTE_STORE.previous_emote_state = "none"
    EMOTE_STORE.skin_state = "purple"
    EMOTE_STORE.previous_skin_state = "purple"
end
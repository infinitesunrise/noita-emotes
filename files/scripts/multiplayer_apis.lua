--tell multiplayer frameworks that we're starting an emote, so other players get informed as well
function send_emote(emote_name)
    --noita-together
    if ModIsEnabled("noita-together") then
        dofile("mods/noita-together/files/store.lua")
        dofile("mods/noita-together/files/scripts/json.lua")
        local payload = { 
            event = "CustomModEvent",
            payload = {
                name = "Emote",
                emote = emote_name
            }
        }
        local queue = json.decode(NT.wsQueue)
        table.insert(queue, payload)
        NT.wsQueue = json.encode(queue)
    end

    --evasia.arena
    if ModIsEnabled("evaisa.arena") then
        dofile("mods/noita-emotes/files/scripts/emote_store.lua")
        EMOTE_STORE.emote_state = emote_name
    end
end

--tell multiplayer frameworks that we're starting an emote, so other players get informed as well
function send_skin_swap(skin_name)
    --noita-together
    if ModIsEnabled("noita-together") then
        dofile("mods/noita-together/files/store.lua")
        dofile("mods/noita-together/files/scripts/json.lua")
        local payload = { 
            event = "CustomModEvent",
            payload = {
                name = "Skin",
                skin = skin_name
            }
        }
        local queue = json.decode(NT.wsQueue)
        table.insert(queue, payload)
        NT.wsQueue = json.encode(queue)
    end

    --evasia.arena
    if ModIsEnabled("evaisa.arena") then
        dofile("mods/noita-emotes/files/scripts/emote_store.lua")
        EMOTE_STORE.skin_state = skin_name
    end
end

--a hack to reset the EMOTE_STORE states for emotes that don't loop 
--(they don't send a second event for emote ending so the store needs to be reset here instead)
function reset_emote_store_for_multiplayer_apis(emote_name)
    if ModIsEnabled("evaisa.arena") then
        dofile("mods/noita-emotes/files/scripts/emote_store.lua")
        EMOTE_STORE.emote_state = emote_name
        EMOTE_STORE.previous_emote_state = emote_name
    end
end
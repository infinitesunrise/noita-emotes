--wrap the gameplay handler's LoadPlayer function to also add the emote system entity to the player
_LoadPlayer = gameplay_handler.LoadPlayer
gameplay_handler.LoadPlayer = function(lobby, data)
    _LoadPlayer(lobby, data)

    --add the emote system entity
    if EntityGetWithName("emotes") == 0 or EntityGetIsAlive(EntityGetWithName("emotes")) then
        --lazy way to find the player since vanilla LoadPlayer doesn't return anything and I don't wanna override it
        local player_id = EntityGetWithTag("player_unit")[1]
        local player_x, player_y = EntityGetTransform(player_id)
        local emote_system = EntityLoad("mods/noita-emotes/files/entities/emotes.xml", player_x, player_y)
        EntityAddChild(player_id, emote_system)
    end
end

--wrap the gameplay handler's SpawnClientPlayer
_SpawnClientPlayer = gameplay_handler.SpawnClientPlayer
gameplay_handler.SpawnClientPlayer = function(lobby, user, data, x, y)
    local client = _SpawnClientPlayer(lobby, user, data, x, y)

    --add an emote system to the client, but give it a new name as to not interfere with local player's emote system
    local client_x, client_y = EntityGetTransform(client)
    local client_emotes = EntityLoad("mods/noita-emotes/files/entities/emotes.xml", client_x, client_y)
    EntitySetName(client_emotes, "emotes_on_client")
    EntityAddChild(client, client_emotes)

    return client
end

gameplay_handler.EmoteClientPlayer = function(user, emote, data)
    if (data.spectator_mode or (GameHasFlagRun("player_is_unlocked") and (not GameHasFlagRun("no_shooting")))) then
        local entity = data.players[tostring(user)].entity
        if (entity ~= nil and EntityGetIsAlive(entity)) then
            local children = EntityGetAllChildren(entity)
            for _, child in ipairs(children) do
                if EntityGetName(child) == "emotes_on_client" then
                    local current_emote_var_comp = EntityGetComponentIncludingDisabled(child, "VariableStorageComponent")[1]
                    ComponentSetValue2(current_emote_var_comp, "value_string", emote)
                    local frames_emoting_var_comp = EntityGetComponentIncludingDisabled(child, "VariableStorageComponent")[4]
                    ComponentSetValue2(frames_emoting_var_comp, "value_int", 0)
                end
            end
        end
    end
end

gameplay_handler.SkinSwapClientPlayer = function(user, skin, data)
    if (data.spectator_mode or (GameHasFlagRun("player_is_unlocked") and (not GameHasFlagRun("no_shooting")))) then
        local entity = data.players[tostring(user)].entity
        if (entity ~= nil and EntityGetIsAlive(entity)) then
            local children = EntityGetAllChildren(entity)
            for _, child in ipairs(children) do
                if EntityGetName(child) == "emotes_on_client" then
                    local skin_var_comp = EntityGetComponentIncludingDisabled(child, "VariableStorageComponent")[8]
                    ComponentSetValue2(skin_var_comp, "value_string", skin)
                end
            end
        end
    end
end

--wrap gameplay handler's Update function to check for changes to the ARENA_STORE stringstore indicating an emote change (ultimate jank)
_Update = gameplay_handler.Update
gameplay_handler.Update = function(lobby, data)
    _Update(lobby, data)
    dofile("mods/noita-emotes/files/scripts/emote_store.lua")
    if EMOTE_STORE.emote_state ~= EMOTE_STORE.previous_emote_state then
        networking.send.emote(lobby, EMOTE_STORE.emote_state)
        EMOTE_STORE.previous_emote_state = EMOTE_STORE.emote_state
    end
    if EMOTE_STORE.skin_state ~= EMOTE_STORE.previous_skin_state then
        networking.send.skin(lobby, EMOTE_STORE.skin_state)
        EMOTE_STORE.previous_skin_state = EMOTE_STORE.skin_state
    end
end

--receiving emotes
networking.receive.emote = function(lobby, message, user, data)
    if (not gameplay_handler.CheckPlayer(lobby, user, data)) then
        return
    end
    local emote = message[1]
    gameplay_handler.EmoteClientPlayer(user, emote, data)
end

--sending emotes
networking.send.emote = function(lobby, emote)
    steamutils.send("emote", { emote }, steamutils.messageTypes.OtherPlayers, lobby, true, true)
end

--receiving skin
networking.receive.skin = function(lobby, message, user, data)
    if (not gameplay_handler.CheckPlayer(lobby, user, data)) then
        return
    end
    local skin = message[1]
    gameplay_handler.SkinSwapClientPlayer(user, skin, data)
end

--sending skin
networking.send.skin = function(lobby, skin)
    steamutils.send("skin", { skin }, steamutils.messageTypes.OtherPlayers, lobby, true, true)
end
function SpawnPlayerGhost(player, userId)
    local ghost = EntityLoad("mods/noita-together/files/entities/ntplayer.xml", 0, 0)
    AppendName(ghost, player.name)
    local vars = EntityGetComponent(ghost, "VariableStorageComponent")
    for _, var in pairs(vars) do
        local name = ComponentGetValue2(var, "name")
        if (name == "userId") then
            ComponentSetValue2(var, "value_string", userId)
        end
        if (name == "inven") then
            ComponentSetValue2(var, "value_string", json.encode(PlayerList[userId].inven))
        end
    end
    if (player.x ~= nil and player.y ~= nil) then
        EntitySetTransform(ghost, player.x, player.y)
    end

    --add an emote system to the ghost, but give it a new name as to not interfere with local player's emote system
    local ghost_x, ghost_y = EntityGetTransform(ghost)
    local ghost_emotes = EntityLoad("mods/noita-emotes/files/entities/emotes.xml", ghost_x, ghost_y)
    EntitySetName(ghost_emotes, "emotes_on_ghost")
    EntityAddChild(ghost, ghost_emotes)
end

function EmotePlayerGhost(data)
    local ghosts = EntityGetWithTag("nt_ghost")
    for _, ghost in pairs(ghosts) do
        local id_comp = get_variable_storage_component(ghost, "userId")
        local userId = ComponentGetValue2(id_comp, "value_string")
        if (userId == data.userId) then
            local children = EntityGetAllChildren(ghost)
            for _, child in ipairs(children) do
                if EntityGetName(child) == "emotes_on_ghost" then
                    local current_emote_var_comp = EntityGetComponentIncludingDisabled(child, "VariableStorageComponent")[1]
                    ComponentSetValue2(current_emote_var_comp, "value_string", data.emote)
                    local frames_emoting_var_comp = EntityGetComponentIncludingDisabled(child, "VariableStorageComponent")[4]
                    ComponentSetValue2(frames_emoting_var_comp, "value_int", 0)
                end
            end
            break
        end
    end
end

function SkinSwapPlayerGhost(data)
    local ghosts = EntityGetWithTag("nt_ghost")
    for _, ghost in pairs(ghosts) do
        local id_comp = get_variable_storage_component(ghost, "userId")
        local userId = ComponentGetValue2(id_comp, "value_string")
        if (userId == data.userId) then
            local children = EntityGetAllChildren(ghost)
            for _, child in ipairs(children) do
                if EntityGetName(child) == "emotes_on_ghost" then
                    local skin_var_comp = EntityGetComponentIncludingDisabled(child, "VariableStorageComponent")[8]
                    print(tostring(data.skin))
                    print(tostring(skin_var_comp))
                    ComponentSetValue2(skin_var_comp, "value_string", data.skin)
                end
            end
            break
        end
    end
end
local version = "2023111800"

if ModIsEnabled("noita-together") then
    ModLuaFileAppend("mods/noita-together/files/scripts/utils.lua", "mods/noita-emotes/files/scripts/nt_utils_appends.lua")
    ModLuaFileAppend("mods/noita-together/files/ws/events.lua", "mods/noita-emotes/files/scripts/nt_events_appends.lua")

    --mangle NT's RequestGameInfo responder to include noita-emotes version in returned mod IDs (makes version visible in desktop client lobby)
    local nt_events_content = ModTextFileGetContent("mods/noita-together/files/ws/events.lua")
    nt_events_content = nt_events_content:gsub(
        "local mods = ModGetActiveModIDs%(%)",
        "local mods = ModGetActiveModIDs() for index, mod in pairs(mods) do if mod == \"noita-emotes\" then mods[index] = \"noita-emotes v" .. tostring(version) .. "\" end end"
    )
    ModTextFileSetContent("mods/noita-together/files/ws/events.lua", nt_events_content)
end

if ModIsEnabled("evaisa.arena") then
    ModLuaFileAppend("mods/evaisa.arena/files/scripts/gamemode/main.lua", "mods/noita-emotes/files/scripts/arena_main_appends.lua")
end

if ModIsEnabled("mnee") then
	ModLuaFileAppend("mods/mnee/bindings.lua", "mods/noita-emotes/mnee.lua")
end

function OnPlayerSpawned(player_id)
    --add the emote system entity
    if EntityGetWithName("emotes") == 0 then
        local emote_system = EntityLoad("mods/noita-emotes/files/entities/emotes.xml", player_x, player_y)
        EntityAddChild(player_id, emote_system)
    end
end